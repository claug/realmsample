package ro.learning.realmadvanceddatabasesample;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import ro.learning.realmadvanceddatabasesample.retrofit.OpenWeatherApi;
import ro.learning.realmadvanceddatabasesample.retrofit.ServiceFactory;
import ro.learning.realmadvanceddatabasesample.retrofit.model.City;

public class JsonSampleActivity extends AppCompatActivity {
    private static final String TAG = JsonSampleActivity.class.getSimpleName();

    @BindView(R.id.tv_json_with_gson)
    TextView tvDefaultRealmAdd;
    @BindView(R.id.tv_json_realm_add_json)
    TextView tvJsonRealmAdd;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_json);
        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Downloading and storing data from JSON ...");
        progressDialog.setCancelable(false);
    }

    @OnClick(R.id.b_json_start)
    void onStartClick() {
        progressDialog.show();
        new GetAndStoreFromJsonAsync().execute();
    }

    private class GetAndStoreFromJsonAsync extends AsyncTask<Void, Void, String> {

        private final String TAG = GetAndStoreFromJsonAsync.class.getSimpleName();

        @Override
        protected String doInBackground(Void... params) {
            OpenWeatherApi api = ServiceFactory.getOpenWeatherService();
            try {
                return api.getRawCitiesByBbox("1,2,62,67,10", "yes", ServiceFactory.WEATHER_API_KEY)
                        .execute().body().string();
            } catch (IOException e) {
                Log.e(TAG, "doInBackground: ", e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);

            JsonParser parser = new JsonParser();
            JsonObject obj = parser.parse(response).getAsJsonObject();
            JsonArray jsonArray = obj.getAsJsonArray("list");
            String jsonArrayAsString = jsonArray.toString();

            extractWithGson(jsonArrayAsString);
            fromJson(jsonArrayAsString);

            progressDialog.dismiss();
        }
    }

    private void extractWithGson(String jsonArray) {
        long startTime = System.currentTimeMillis();

        Type listType = new TypeToken<ArrayList<City>>() {}.getType();
        List<City> cities = new Gson().fromJson(jsonArray, listType);

        RealmConfiguration.Builder builder = new RealmConfiguration.Builder(getApplicationContext());
        Realm realm = Realm.getInstance(builder.build());
        realm.beginTransaction();
        realm.deleteAll();
        realm.commitTransaction();

        realm.beginTransaction();
        realm.copyToRealm(cities);
        realm.commitTransaction();
        realm.close();

        long endTime = System.currentTimeMillis();
        tvDefaultRealmAdd.setText("Realm default: " + (endTime - startTime) + " ms");
    }

    private void fromJson(String jsonArray) {
        long startTime = System.currentTimeMillis();

        RealmConfiguration.Builder builder = new RealmConfiguration.Builder(getApplicationContext());
        Realm realm = Realm.getInstance(builder.build());
        realm.beginTransaction();
        realm.deleteAll();
        realm.commitTransaction();

        realm.beginTransaction();
        realm.createAllFromJson(City.class, jsonArray);
        realm.commitTransaction();
        realm.close();

        long endTime = System.currentTimeMillis();
        tvJsonRealmAdd.setText("Realm from json: " + (endTime - startTime) + " ms");
    }

}
