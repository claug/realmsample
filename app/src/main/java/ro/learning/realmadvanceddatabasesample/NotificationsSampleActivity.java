package ro.learning.realmadvanceddatabasesample;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import ro.learning.realmadvanceddatabasesample.retrofit.OpenWeatherApi;
import ro.learning.realmadvanceddatabasesample.retrofit.ServiceFactory;
import ro.learning.realmadvanceddatabasesample.retrofit.model.Cities;
import ro.learning.realmadvanceddatabasesample.retrofit.model.City;

public class NotificationsSampleActivity extends AppCompatActivity {
    private static final String TAG = NotificationsSampleActivity.class.getSimpleName();

    private static final String BBOX_VALUE = "1,2,12,17,10";

    @BindView(R.id.b_notifications_change_first)
    Button bChangeFirst;

    private List<City> cities;

    private RecyclerView rvCities;
    private WeatherRecyclerViewAdapter rvCitiesAdapter;
    private Realm realm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        ButterKnife.bind(this);

        RealmConfiguration.Builder builder = new RealmConfiguration.Builder(getApplicationContext());
        realm = Realm.getInstance(builder.build());

        cities = new LinkedList<>();
        rvCities = (RecyclerView) findViewById(R.id.rv_cities_temperature);
        rvCities.setLayoutManager(new GridLayoutManager(this, 1));
        rvCitiesAdapter = new WeatherRecyclerViewAdapter(cities);
        rvCities.setAdapter(rvCitiesAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();

        new AddAllCitiesTask().execute();

    }

    @OnClick(R.id.b_notifications_sync)
    void onSynchronizeClick() {
        new SyncCitiesTask().execute();
    }

    @OnClick(R.id.b_notifications_change_first)
    void onChangeFirstClick() {
        realm.beginTransaction();
        cities.get(0).getMain().setTemp(cities.get(0).getMain().getTemp() + 2);
        realm.commitTransaction();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        realm.removeAllChangeListeners();
        realm.close();
    }

    class SyncCitiesTask extends AsyncTask<Void, Void, Cities> {

        private final String TAG = SyncCitiesTask.class.getSimpleName();

        @Override
        protected Cities doInBackground(Void... params) {
            OpenWeatherApi api = ServiceFactory.getOpenWeatherService();
            try {
                return api.getCitiesByBbox(BBOX_VALUE, "yes", ServiceFactory.WEATHER_API_KEY)
                        .execute().body();
            } catch (IOException e) {
                Log.d(TAG, "doInBackground: ");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Cities cities) {
            super.onPostExecute(cities);

            realm.beginTransaction();
            realm.copyToRealmOrUpdate(cities.getList());
            realm.commitTransaction();
        }
    }

    RealmChangeListener realmChangeListener = new RealmChangeListener<Realm>() {

        @Override
        public void onChange(Realm realm) {
            Log.d(TAG, "onChange: updated database");
            RealmResults<City> results = realm.where(City.class).findAll();
            cities.clear();
            for (int i = 0; i < results.size(); i++) {
                cities.add(results.get(i));
            }
            rvCitiesAdapter.notifyDataSetChanged();
            bChangeFirst.setVisibility(View.VISIBLE);
        }
    };

    RealmChangeListener firstCityChangeListener = new RealmChangeListener<City>() {
        @Override
        public void onChange(City city) {
            rvCitiesAdapter.notifyItemChanged(0);
        }
    };

    class AddAllCitiesTask extends AsyncTask<Void, Void, Cities> {


        private final String TAG = AddAllCitiesTask.class.getSimpleName();

        @Override
        protected Cities doInBackground(Void... params) {
            OpenWeatherApi api = ServiceFactory.getOpenWeatherService();
            try {
                return api.getCitiesByBbox(BBOX_VALUE, "yes", ServiceFactory.WEATHER_API_KEY)
                        .execute().body();
            } catch (IOException e) {
                Log.d(TAG, "doInBackground: ");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Cities responseCities) {
            super.onPostExecute(responseCities);

            cities.addAll(responseCities.getList());
            rvCitiesAdapter.notifyDataSetChanged();

            realm.beginTransaction();
            realm.deleteAll();
            realm.commitTransaction();

            realm.beginTransaction();
            realm.copyToRealm(responseCities.getList());
            realm.addChangeListener(realmChangeListener);

            City firstCity = realm.where(City.class).findFirst();
            firstCity.addChangeListener(firstCityChangeListener);

            realm.commitTransaction();
        }
    }

}
