package ro.learning.realmadvanceddatabasesample;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ro.learning.realmadvanceddatabasesample.retrofit.model.City;

public class WeatherRecyclerViewAdapter
        extends RecyclerView.Adapter<WeatherRecyclerViewAdapter.WeatherViewHolder> {

    private final List<City> cities;

    public WeatherRecyclerViewAdapter (List<City> cities) {
        this.cities = cities;
    }

    @Override
    public WeatherViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.weather_item_view,
                parent, false);

        return new WeatherViewHolder(view);
    }

    @Override
    public void onBindViewHolder(WeatherViewHolder holder, int position) {
        City city = cities.get(position);
        holder.tvCity.setText(city.getName());
        holder.tvTemperature.setText(city.getMain().getTemp() + " degrees");
    }

    @Override
    public int getItemCount() {
        return cities.size();
    }

    class WeatherViewHolder extends RecyclerView.ViewHolder {

        private final TextView tvCity;
        private final TextView tvTemperature;

        public WeatherViewHolder(View itemView) {
            super(itemView);

            tvCity = (TextView) itemView.findViewById(R.id.tv_city_name);
            tvTemperature = (TextView) itemView.findViewById(R.id.tv_temperature);
        }
    }
}
