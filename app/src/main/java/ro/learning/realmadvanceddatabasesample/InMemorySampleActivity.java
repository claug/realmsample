package ro.learning.realmadvanceddatabasesample;

import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import ro.learning.realmadvanceddatabasesample.retrofit.OpenWeatherApi;
import ro.learning.realmadvanceddatabasesample.retrofit.ServiceFactory;
import ro.learning.realmadvanceddatabasesample.retrofit.model.Cities;
import ro.learning.realmadvanceddatabasesample.retrofit.model.City;

public class InMemorySampleActivity extends AppCompatActivity {
    private static final String TAG = InMemorySampleActivity.class.getSimpleName();

    @BindView(R.id.et_in_memory_city)
    EditText etCityName;

    @BindView(R.id.tv_in_memory_default)
    TextView tvDefaultResults;

    @BindView(R.id.tv_in_memory_memory)
    TextView tvInMemoryResults;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_in_memory);
        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
    }

    @OnClick(R.id.b_in_memory_search)
    void onSearchClick() {
        progressDialog.setMessage("Downloading and saving weather data ...");
        progressDialog.show();
        (new GetAndStoreAsync()).execute(etCityName.getText().toString());
    }

    private class GetAndStoreAsync extends AsyncTask<String, Void, Cities> {

        private final String TAG = GetAndStoreAsync.class.getSimpleName();

        private String cityName;

        @Override
        protected Cities doInBackground(String... params) {
            cityName = params[0];
            OpenWeatherApi api = ServiceFactory.getOpenWeatherService();
            try {
                return api.getCitiesByBbox("1,2,62,67,10", "yes", ServiceFactory.WEATHER_API_KEY)
                        .execute().body();
            } catch (IOException e) {
                Log.e(TAG, "doInBackground: ", e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Cities cities) {
            super.onPostExecute(cities);
            searchInMemory(cities, cityName);
            searchDefault(cities, cityName);
            progressDialog.dismiss();
        }
    }

    private void searchDefault(Cities cities, String cityName) {
        String result = creatDatabaseAndQueryFor(cities, cityName, false);
        tvDefaultResults.setText("DEFAULT \n " + result);
    }

    private void searchInMemory(Cities cities, String cityName) {
        String result = creatDatabaseAndQueryFor(cities, cityName, true);
        tvInMemoryResults.setText("IN-MEMORY \n " + result);
    }

    private String creatDatabaseAndQueryFor(Cities cities, String cityName, boolean inMemory) {
        // Create a RealmConfiguration which is to locate Realm file in package's "files" directory.
        RealmConfiguration.Builder builder = new RealmConfiguration.Builder(getApplicationContext());

        if (inMemory) {
            builder.inMemory();
        }

        // Get a Realm instance for this thread
        Realm realm = Realm.getInstance(builder.build());
        realm.beginTransaction();
        realm.deleteAll();
        realm.commitTransaction();

        long startTime = System.currentTimeMillis();
        realm.beginTransaction();
        realm.copyToRealm(cities.getList());
        realm.commitTransaction();
        realm.close();
        long addEndTime = System.currentTimeMillis();

        System.gc();
        long beforeQueryMemmorySize = getUsedMemorySize();
        // Build the query looking at all users:
        realm = Realm.getInstance(builder.build());
        RealmQuery<City> query = realm.where(City.class);
        long duringQueryMemorySize = getUsedMemorySize();
        // Add query conditions:
        query.equalTo("name", cityName);
        realm.close();
        long queryEndTime = System.currentTimeMillis();

        StringBuilder statsBuilder = new StringBuilder();
        statsBuilder.append("Add operation of " + cities.getList().size() + " entries: " + (addEndTime - startTime) + " ms. \n");
        statsBuilder.append("Query operation: " + (queryEndTime - addEndTime) + "ms.\n");
        statsBuilder.append("Memory used during query: " + (duringQueryMemorySize - beforeQueryMemmorySize) / 1024 + " KB.");

        return statsBuilder.toString();
    }

    public long getUsedMemorySize() {
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(mi);

        return (mi.totalMem - mi.availMem);
    }

}
