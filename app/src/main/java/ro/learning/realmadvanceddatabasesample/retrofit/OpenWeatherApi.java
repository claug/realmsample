package ro.learning.realmadvanceddatabasesample.retrofit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ro.learning.realmadvanceddatabasesample.retrofit.model.Cities;

public interface OpenWeatherApi {

    @GET("box/city?")
    Call<Cities> getCitiesByBbox(@Query("bbox") String bbox,
                                 @Query("cluster") String cluster,
                                 @Query("APPID") String appId);

    @GET("box/city?")
    Call<ResponseBody> getRawCitiesByBbox(@Query("bbox") String bbox,
                                          @Query("cluster") String cluster,
                                          @Query("APPID") String appId);

}
