package ro.learning.realmadvanceddatabasesample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.b_main_in_memory)
    void onInMemoryClick() {
        startActivity(new Intent(this, InMemorySampleActivity.class));
    }

    @OnClick(R.id.b_main_json)
    void onJsonClick() {
        startActivity(new Intent(this, JsonSampleActivity.class));
    }

    @OnClick(R.id.b_main_notifications)
    void onNotificationsClick() {
        startActivity(new Intent(this, NotificationsSampleActivity.class));
    }
}
